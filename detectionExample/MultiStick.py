import sys
graph_folder="../"
if sys.version_info.major < 3 or sys.version_info.minor < 4:
    print("Please using python3.4 or greater!")
    exit(1)

if len(sys.argv) > 1:
    graph_folder = sys.argv[1]

from mvnc import mvncapi as mvnc
import numpy as np
import cv2
from os import system
import io, time
from os.path import isfile, join
from queue import Queue
from threading import Thread, Event, Lock
import re
from time import sleep
from Visualize import *
from libpydetector import YoloDetector
from skimage.transform import resize

write_output = False
mvnc.SetGlobalOption(mvnc.GlobalOption.LOG_LEVEL, 2)


#cam = cv2.VideoCapture(0)
#cam = cv2.VideoCapture('/home/pi/YoloV2NCS/detectionExample/xxxx.mp4')

camera_device_num = 0
#camera_device_num = "/home/pi/Desktop/dataset/DVD_204119_0.mpg"
#camera_device_num = '/home/pi/Desktop/dataset/31-Jul-2017_20-09-01_2000fr.mp4'
#camera_device_num = '/home/pi/Desktop/dataset/31-Jul-2017_20-48-26_2000fr.mp4'
#camera_device_num = '/home/pi/Desktop/dataset/auto-01.mp4'
while(True):
    cam = cv2.VideoCapture(camera_device_num)
    if cam.isOpened():
        break;
    camera_device_num = camera_device_num + 1
    if camera_device_num > 10:
        break;

if cam.isOpened() != True:
    print("Camera/Movie Open Error!!!")
    quit()


widowWidth = 416
windowHeight = 234

cam.set(cv2.CAP_PROP_FRAME_WIDTH, widowWidth)
cam.set(cv2.CAP_PROP_FRAME_HEIGHT, windowHeight)

#cam.set(cv2.CAP_PROP_FRAME_WIDTH, 416)
#cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 234)
#cam.set(cv2.CAP_PROP_FRAME_WIDTH, 320)
#cam.set(cv2.CAP_PROP_FRAME_HEIGHT, 240)

#(WEBカメラのみ)
#捨てるフレームが増えてカクつきが増える代わりに実像とプレディクション枠のズレを軽減
#cam.set(cv2.CAP_PROP_FPS, 100)

if write_output:
    fourcc = cv2.VideoWriter_fourcc(*'MJPG')
    print ("fourcc", fourcc)


    out = cv2.VideoWriter(filename =
                      '/media/pi/New01/dataset/multistick_ssd_output_'
                      + strftime("%a%d%b%Y_%H%M%S", localtime()) + '.avi',
                      fourcc=cam_fourcc, fps=cam_fps, frameSize=(cam_width, cam_height))



devices = mvnc.EnumerateDevices()
if len(devices) == 0:
    print("No devices found")
    quit()
print(len(devices))

devHandle   = []
graphHandle = []

with open(join(graph_folder, "graph"), mode="rb") as f:
    graph = f.read()

for devnum in range(len(devices)):
    devHandle.append(mvnc.Device(devices[devnum]))
    devHandle[devnum].OpenDevice()

    #opt = devHandle[devnum].GetDeviceOption(mvnc.DeviceOption.OPTIMISATION_LIST)

    graphHandle.append(devHandle[devnum].AllocateGraph(graph))
    graphHandle[devnum].SetGraphOption(mvnc.GraphOption.ITERATIONS, 1)
    iterations = graphHandle[devnum].GetGraphOption(mvnc.GraphOption.ITERATIONS)

    #dim = (416,416)
    dim = (widowWidth, widowWidth)
    #dim = (320,320)
    blockwd = 12
    #blockwd = 9
    wh = blockwd*blockwd
    targetBlockwd = 12
    #targetBlockwd = 9
    classes = 20
    threshold = 0.2
    nms = 0.4

print("\nLoaded Graphs!!!")

lock = Lock()
frameBuffer = []
results = Queue()
detector = YoloDetector(1)

def camThread(cam, lock, buff, resQ):
    lastresults = None
    print("press 'q' to quit!\n")
    append = buff.append
    get = resQ.get

    failure = 0
    while failure < 100:
        s, img = cam.read()
        t = time.time()
        if not s:
            failure += 1
            print("Could not get frame")
            continue

        failure = 0
        lock.acquire()
        if len(buff)>10:
            for i in range(10):
                buff.pop()
        append((t,img))
        lock.release()
        results = None

        try:
            results = get(False)
        except:
            pass

        if results == None:
            if lastresults == None:
                pass
            else:
                imdraw = Visualize(img, lastresults, t)
                cv2.imshow('DEMO', imdraw)
                #write video
                if write_output:
                    out.write(imdraw)
        else:
            imdraw = Visualize(img, results, t)
            cv2.imshow('DEMO', imdraw)
            #write video
            if write_output:
                out.write(imdraw)
            lastresults = results

        key = cv2.waitKey(1) & 0xFF

        if key == ord("q"):
            break

    cv2.destroyAllWindows()

    lock.acquire()
    while len(buff) > 0:
        del buff[0]
    lock.release()

def inferencer(results, lock, frameBuffer, handle, devnum):
    failure = 0
    sleep(1)
    while failure < 100:

        lock.acquire()
        if len(frameBuffer) == 0:
            lock.release()
            failure += 1
            sleep(0.1)
            continue

        (t, imgbuff) = frameBuffer[-1]
        img = imgbuff.copy()
        del frameBuffer[-1]
        failure = 0
        lock.release()

        imgw = img.shape[1]
        imgh = img.shape[0]
        im,offx,offy = PrepareImage(img, dim)
        handle.LoadTensor(im.astype(np.float16), 'user object')
        out, userobj = handle.GetResult()
        out = Reshape(out, dim)
        internalresults = detector.Detect(out.astype(np.float32), int(out.shape[0]/wh), blockwd, blockwd, classes, imgw, imgh, threshold, nms, targetBlockwd)
        pyresults = [BBox(x) for x in internalresults]
        for bbox in pyresults:
            bbox.set(devnum, t)
        results.put(pyresults)

def PrepareImage(img, dim):
    imgw = img.shape[1]
    imgh = img.shape[0]
    imgb = np.empty((dim[0], dim[1], 3))
    imgb.fill(0.5)

    if imgh/imgw > dim[1]/dim[0]:
        neww = int(imgw * dim[1] / imgh)
        newh = dim[1]
    else:
        newh = int(imgh * dim[0] / imgw)
        neww = dim[0]
    offx = int((dim[0] - neww)/2)
    offy = int((dim[1] - newh)/2)

    imgb[offy:offy+newh,offx:offx+neww,:] = resize(img.copy()/255.0,(newh,neww),1)
    im = imgb[:,:,(2,1,0)]
    return im,offx,offy

def Reshape(out, dim):
    shape = out.shape
    out = np.transpose(out.reshape(wh, int(shape[0]/wh)))  
    out = out.reshape(shape)
    return out

class BBox(object):
    def __init__(self, bbox):
        self.left = bbox.left
        self.top = bbox.top
        self.right = bbox.right
        self.bottom = bbox.bottom
        self.confidence = bbox.confidence
        self.objType = bbox.objType
        self.name = bbox.name

    def set(self, devnum, t):
        self.t = t
        self.devnum = devnum

threads = []

camT = Thread(target=camThread, args=(cam, lock, frameBuffer, results))
camT.start()
threads.append(camT)

for devnum in range(len(devices)):
  t = Thread(target=inferencer, args=(results, lock, frameBuffer, graphHandle[devnum], devnum))
  t.start()
  threads.append(t)

for t in threads:
  t.join()

for devnum in range(len(devices)):
  graphHandle[devnum].DeallocateGraph()
  devHandle[devnum].CloseDevice()

print("\n\nFinished\n\n")

